using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Http;

namespace Photoguru.Models
{
    public class User
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Picture { get; set; }
        public string Nickname { get; set; }
        public string Sub { get; set; }
        public ICollection<Photo> Photos { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public ICollection<PhotoLike> PhotoLikes { get; set; }
        public ICollection<CommentLike> CommentLikes { get; set; }

        public User() 
        {
            Photos = new List<Photo>();
            Comments = new List<Comment>();
            PhotoLikes = new List<PhotoLike>();
            CommentLikes = new List<CommentLike>();
        }
    }
}
