using System;
using System.IO;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Photoguru.Models
{
    public class PhotoLike
    {
        public int Id { get; set; }
        public int PhotoId { get; set; }
        [JsonIgnore]
        public Photo Photo { get; set; }
        public string UserId { get; set; }
        [JsonIgnore]
        public User User { get; set; }
    }
}
