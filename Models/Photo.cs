using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Photoguru.Models
{
    public class Photo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string UniqueName { get; set; }
        public string RelativePath { get; set; }
        public string AbsolutePath { get; set; }
        public int Length { get; set; }
        public bool ImageSaved { get; set; }
        public DateTime? CreationDate { get; set; }
        public string UserId { get; set; }
        [JsonIgnore]
        public User User { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public ICollection<PhotoLike> Likes { get; set; }
      
        public static Photo Create(IFormFile file) 
        {
            string uniqueString = Guid.NewGuid().ToString();
            string extension = Path.GetExtension(file.FileName);

            var photo = new Photo
            {
                UniqueName = $"{uniqueString}{extension}",
                Name = Path.GetFileNameWithoutExtension(file.FileName),
                Length = (int) file.Length
            };

            return photo;
        }
    }
}
