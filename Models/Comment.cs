using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Photoguru.Models
{
    public class Comment
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public int PhotoId { get; set; }
        public string UserId { get; set; }
        [JsonIgnore]
        public User User { get; set; }
        public ICollection<CommentLike> Likes { get; set; }

        public Comment() 
        {
            Likes = new List<CommentLike>();
        }
    }
}
