# photoguru

## Important :warning: 

NEVER USE GITHUB GUI TO RESOLVE CONFLICTS :warning: 

## About

### Description

Application for uploading, viewing and commenting images for certain workspace - an (potentially) scoped equivalent of Instagram

### Technology stack

#### Backend

| Technology               |   Version   |
| :----------------------- | :---------: |
| C#                       |     7.1     |
| asp.net core             |    2.0.5    |
| Entity Framework core    |    2.0.1    |
| NpgSQL (Postgres for EF) |    2.0.0    |

#### Frontend

| Technology               |   Version   |
| :----------------------- | :---------: |
| Typescript               |    2.6.2    |
| Node.js                  |    9.3.x    |
| Angular                  |    5.2.0    |
| Redux                    |    3.7.2    |
| angular-redux            |    7.1.0    |
| redux-observable         |    0.18.0   |
| rxjs                     |    5.5.6    |
| webpack                  |    3.10.0   |
| sass (as node-sass)      |    4.5.3    |
| haml                     |    0.4.3    |

## Project Setup

### External dependencies

First, make sure you have all the dependencies installed:

* [`dotnet SDK`](https://www.microsoft.com/net/download/macos/): most up-to-date version (at least `2.0.0`)
* [`node`](https://nodejs.org/en/): most up-to-date version - at least `8.x.x` (LTS)

We suggest you to use [`nvm`](https://github.com/creationix/nvm) as your Node binary. Project is meant to run fine both on LTS and most recent version. However If you try to run it on a different major version, you might stumble upon `node-sass` errors - as the prebuilt binaries (referred in npm-shrinkwrap.json) are for that exact version (LTS) only. See [troubleshooting](#troubleshooting) section for the solution.

<!--
### Automated (recommended)

```bash
./setup
```

Update `appsettings.json` and `ClientApp/app/assets/config/config.json` according to your needs.

Ask your teammates about any secret tokens.
-->

### Manual

#### Dependencies

```bash
dotnet restore
npm install
```

#### Config files

Copy sample config files:

```bash
cp appsettings.example.json appsettings.json
cp ClientApp/app/assets/config/config.example.json ClientApp/app/assets/config/config.json
```

Update config files according to your needs.

Ask your teammates about any secret tokens.

#### Environment variables

Set asp.net core environment variable:
```bash
execute export ASPNETCORE_ENVIRONMENT=Development
```

## Running

### Command line:

Just execute command
```bash
dotnet run
```
The website will be visible under url: `http://localhost:5000`

Project uses webpack's Hot Module Replacement so all of your changes to typescript/sass/haml/html files will be automatically visible in the browser (without need of site reload)

Alternatively if you want to have auto-update also for the backend code, please execute as follow:
```bash
dotnet watch run
```

### VS Code:
With the first usage VS Code will try to automatically resolve all external dependencies.

To start debuggin the application just press `F5` while being in VS Code editor.

## Conventions

### Backend

Please follow this [guideline](http://www.dofactory.com/reference/csharp-coding-standards) for basic C# conventions

### Frontend

Angular: please follow [official angular styleguide](https://angular.io/guide/styleguide)

## Troubleshooting

Q: `Module build failed: Error: Node Sass does not yet support your current environment: OS X 64-bit with Unsupported runtime (59) For more information on which environments are supported please see:`

A: Run `npm rebuild --force node-sass` and `npm install`.
