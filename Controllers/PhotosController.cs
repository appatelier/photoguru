using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Photoguru.Data.Requests;
using Photoguru.Data.Services;
using Photoguru.Data.Services.Resources;
using Photoguru.Data.Services.Uploaders;
using Photoguru.Models;

namespace Photoguru.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class PhotosController : AuthenticatedController
    {
        private readonly PhotosService _photosService;
        private readonly ImageService _imageService;

        public PhotosController(PhotosService photosService, ImageService imageService, UsersService usersService)
        : base(usersService)
        {
            _photosService = photosService;
            _imageService = imageService;
        }

        [HttpGet]
        public async Task<PaginatedList<Photo>> Index()
        {
            var pagination = new Pagination
            {
                PageIndex = Request.Headers.GetValue("pageIndex").ToNullableInt(),
                PerPage = Request.Headers.GetValue("perPage").ToNullableInt(),
            };

            return await _photosService.GetPaginated(pagination);
        }

        [HttpGet("{id}")]
        public Photo Get(int id) => _photosService.Get(id);

        [HttpPost]
        public Photo Post([FromBody]Photo photo) {
            photo.UserId = CurrentUser?.Id;
            return _photosService.Add(photo);
        }
            

        [HttpPut("{id}")]
        public Photo Put([FromBody]Photo photo) => _photosService.Update(photo);

        [HttpDelete("{id}")]
        public void Delete([FromBody]Photo photo) => _photosService.Delete(photo);

        [HttpDelete("[action]")]
        public void DeleteAll() => _photosService.DeleteAll();
        
        [HttpPost("[action]")]
        public async Task<IActionResult> Upload(IFormFile file)
        {
            if (file == null || file.Length == 0) return NoContent();

            var photo = await _imageService.SaveAsync(file);
            var savedPhoto = _photosService.Add(photo);

            return Ok(savedPhoto);
        }

        [HttpGet("[action]/{id}")]
        public IActionResult DetectLabels(int id)
        {
            var photo = _photosService.Get(id);
            if (photo == null) return NotFound();

            var labels = _imageService.DetectLabels(photo);

            return Ok(labels);
        }
    }
}
