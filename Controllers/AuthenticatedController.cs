using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Photoguru.Data.Services.Resources;
using Photoguru.Models;

namespace Photoguru.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public abstract class AuthenticatedController : Controller
    {
        protected readonly UsersService _usersService;

        public AuthenticatedController(UsersService usersService) => _usersService = usersService;

        protected User CurrentUser
        {
            get
            {
                var id = User.Claims.First(claim => claim.Properties.Any(prop => prop.Value == "sub")).Value;
                return _usersService.Get(id);
            }
        }
    }
}
