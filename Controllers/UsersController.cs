using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Photoguru.Data.Services.Resources;
using Photoguru.Models;

namespace Photoguru.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class UsersController : AuthenticatedController
    {
        public UsersController(UsersService usersService) : base(usersService) {}

        [HttpGet]
        public IEnumerable<User> Index() => _usersService.GetAll();

        [HttpPost("[action]")]
        public User Update([FromBody]User user) {
            if (user == null || CurrentUser.Id != user.Id) return null;

            return _usersService.GetAll().Any(u => u.Id == user.Id)
                ? _usersService.Update(user)
                : _usersService.Add(user);
        }

        [HttpGet("[action]")]
        public User Current() => CurrentUser;

        [HttpGet("{id}")]
        public User Get(int id) => _usersService.Get(id);

        [HttpDelete("{id}")]
        public void Delete([FromBody]User user) => _usersService.Delete(user);

        [HttpDelete("[action]")]
        public void DeleteAll() => _usersService.DeleteAll();
    }
}
