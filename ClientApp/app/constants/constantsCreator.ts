import { IResourceConstants, ResourceType } from '../models/resourceConstants';

export const constantsCreator = (id: ResourceType, endpoint: string = '') =>
  ({
    id,
    endpoint,
    indexRequest: `${id}/index/request`,
    indexSuccess: `${id}/index/success`,
    indexFailure: `${id}/index/failure`,
  } as IResourceConstants);
