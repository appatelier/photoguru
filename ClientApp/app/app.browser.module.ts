import {AuthService} from './services/auth/auth.service';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { AppModuleShared } from './app.shared.module';
import { AppComponent } from './components/app/app.component';
import { AuthGuard } from './services/auth/authGuard.service';
import { AuthorizedHttpClient } from './services/api/authorizedHttpClient.service';
import { ApiHelper } from './services/api/apiHelper.service';
import { ObjectConverterService } from './services/helpers/objectConverter.service';
import { ApiActionsDispatcher } from './services/api/apiActionsDispatcher';
import { IAppConfig } from './models/settings/appConfig';

const config = require("./assets/config/config.json");

export const getConfig = () => config as IAppConfig;
export const getBaseUrl = () => document.getElementsByTagName('base')[0].href;

@NgModule({
  bootstrap: [ AppComponent ],
  imports: [
    BrowserModule,
    AppModuleShared,
    FormsModule,
    HttpClientModule,
    InfiniteScrollModule,
  ],
  providers: [
    { provide: 'BASE_URL', useFactory: getBaseUrl },
    { provide: 'APP_CONFIG', useFactory: getConfig },
    AuthService,
    AuthGuard,
    AuthorizedHttpClient,
    ApiHelper,
    ApiActionsDispatcher,
    ObjectConverterService
  ]
})
export class AppModule {
}
