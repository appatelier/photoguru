import { Component, OnInit } from '@angular/core';
import { select, NgRedux } from '@angular-redux/store';
import { Observable } from 'rxjs/Observable';

import {AuthService} from '../../services/auth/auth.service';
import { IAppState } from '../../models/appState';
import { IUser } from '../../models/domain/user';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  profile: IUser;

  constructor(
    public auth: AuthService,
    private ngRedux: NgRedux<IAppState>) {}

  async ngOnInit() {
    if (!this.auth.currentUser) {
      await this.auth.fetchProfile();
    }

    this.profile = this.auth.currentUser;
  }
}
