import { Component } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
const src = require("../../assets/images/chwala-netguru.png");

@Component({
  selector: 'unauthorized',
  styleUrls: ['./unauthorized.component.scss'],
  templateUrl: './unauthorized.component.html.hamlc'
})
export class UnauthorizedComponent {
  imageSrc = src;
  constructor(public auth: AuthService) {}
}
