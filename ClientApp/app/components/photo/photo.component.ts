import { Component, OnDestroy, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs/Rx';

import { IPhoto } from '../../models/domain/photo';
import { PhotosService } from '../../services/photos.service';

@Component({
  selector: 'photo',
  templateUrl: './photo.component.html.hamlc',
  styleUrls: ['./photo.component.scss'],
  providers: [PhotosService]
})
export class PhotoComponent implements OnInit, OnDestroy {
  private paramsSub: Subscription;
  photo: IPhoto | null = null;
  photoLabels: Observable<string[]>;

  constructor(
    private route: ActivatedRoute,
    private photosService: PhotosService,
    private ngZone: NgZone,
    private router: Router) {}

  public ngOnInit(): void {
    this.paramsSub = this.route.params.subscribe(params => {
      let id = Number(params['id']);
      this.photosService.getPhoto(id).subscribe(photo => this.photo = photo);
    });
  }

  public ngOnDestroy(): void {
    this.paramsSub && this.paramsSub.unsubscribe();
  }

  delete() {
    if(!this.photo) return;
    this.photosService.delete(this.photo).subscribe(response => this.goToList());
  }

  goToList = () => this.ngZone.run(() => this.router.navigate(['photos']));
}
