import { Component, Inject, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { FileUploader } from 'ng2-file-upload';

import { IPhoto } from '../../models/domain/photo';
import { PhotosService } from '../../services/photos.service';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'uploader',
  templateUrl: './uploader.component.html.hamlc',
  styleUrls: ['./uploader.component.scss'],
  providers: [PhotosService]
})
export class UploaderComponent {
  private readonly allowedTypes: string[];
  public uploader: FileUploader;
  public uploaderDropZoneHover: boolean;
  public uploadedPhoto: IPhoto | null;

  constructor(
    @Inject('BASE_URL') baseUrl: string,
    private photosService: PhotosService,
    private router: Router,
    private ngZone: NgZone,
    private authService: AuthService
  ) {
    this.allowedTypes = ['image/png', 'image/jpeg'];
    this.uploaderDropZoneHover = false;
    this.uploader = new FileUploader({
      autoUpload: true,
      allowedMimeType: this.allowedTypes,
      url: `${baseUrl}api/Photos/Upload`,
      headers: [{ name: 'Authorization', value: 'Bearer ' + this.authService.authToken }],
    });

    this.uploader.onSuccessItem = (item, response, status, headers) => {
        this.uploadedPhoto = JSON.parse(response) as IPhoto;
        this.selectPhoto(this.uploadedPhoto);
    }
  }

  public clearPhoto() {
    this.uploadedPhoto = null;
  }

  public fileOverBase(e: any): void {
    this.uploaderDropZoneHover = e;
  }

  selectPhoto = (photo: IPhoto) => this.ngZone.run(() => this.router.navigate(['photo', photo.id]));
}
