import {AuthService} from '../../services/auth/auth.service';
import { Component, OnInit, NgZone, ChangeDetectorRef } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { select } from '@angular-redux/store';

import { IPhoto } from '../../models/domain/photo';
import { PhotosService } from '../../services/photos.service';
import { PhotosActions } from '../../actions/photos';
import { IListResource } from '../../models/domain/listResource';
import { IPagination } from '../../models/requests/pagination';
import { Router } from '@angular/router';

@Component({
  selector: 'photos',
  templateUrl: './photos.component.html.hamlc',
  styleUrls: ['./photos.component.scss'],
  providers: [PhotosActions]
})
export class PhotosComponent implements OnInit {
  @select() readonly photos$: Observable<IListResource<IPhoto>>;
  pagination: IPagination;
  photos: IPhoto[] = [];

  constructor(
    private PhotosActions: PhotosActions,
    private ref:ChangeDetectorRef,
    private ngZone: NgZone,
    private router: Router,
    public auth: AuthService) {}

  ngOnInit(): void {
    this.PhotosActions.index();
    
    this.photos$.subscribe(photos => {
      this.pagination = photos.pagination;
      photos.items.forEach(photo => photo.style = {'background-image': `url(${photo.absolutePath})`});
      this.photos = photos.items.filter(photo => photo.absolutePath);
      this.ref.detectChanges();
    });
  }
  
  onScroll() {
    this.fetchMore();
  }

  selectPhoto = (photo: IPhoto) => this.ngZone.run(() => this.router.navigate(['photo', photo.id]));

  private fetchMore() {
    if(!this.pagination.hasNextPage) return;    
    this.pagination.pageIndex++;
    this.PhotosActions.index(this.pagination);
  }
}
