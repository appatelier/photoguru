import { Routes } from '@angular/router';
import {CallbackComponent} from './components/callback/callback.component';
import {PhotoComponent} from './components/photo/photo.component';
import {PhotosComponent} from './components/photos/photos.component';
import {UploaderComponent} from './components/uploader/uploader.component';
import { ProfileComponent } from './components/profile/profile.component';
import { UnauthorizedComponent } from './components/unauthorized/unauthorized.component';
import { AuthGuard } from './services/auth/authGuard.service';

export const ROUTES: Routes = [
  { path: 'photos', component: PhotosComponent, canActivate:[AuthGuard] },
  { path: 'photo/:id', component: PhotoComponent, canActivate:[AuthGuard] },
  { path: 'profile', component: ProfileComponent, canActivate:[AuthGuard] },
  { path: 'uploader', component: UploaderComponent, canActivate:[AuthGuard] },
  { path: 'callback', component: CallbackComponent },
  { path: 'unauthorized', component: UnauthorizedComponent },
  { path: '', redirectTo: 'photos', pathMatch: 'full' },
  { path: '**', redirectTo: 'photos' }
];
