import {AuthService} from './services/auth/auth.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { FileDropDirective, FileSelectDirective } from 'ng2-file-upload';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

import { NgReduxModule, NgRedux, DevToolsExtension } from '@angular-redux/store';
import { apiMiddleware } from 'redux-api-middleware';

import { ROUTES } from './app.routes';

import { AppComponent } from './components/app/app.component';
import { CallbackComponent } from './components/callback/callback.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { PhotoComponent } from './components/photo/photo.component';
import { PhotosComponent } from './components/photos/photos.component';
import { UploaderComponent } from './components/uploader/uploader.component';
import { ProfileComponent } from './components/profile/profile.component';
import { UnauthorizedComponent } from './components/unauthorized/unauthorized.component';
import { IAppState } from './models/appState';
import { rootReducer, INITIAL_STATE } from './reducers/root';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    UploaderComponent,
    PhotosComponent,
    PhotoComponent,
    FileSelectDirective,
    FileDropDirective,
    CallbackComponent,
    ProfileComponent,
    UnauthorizedComponent,
  ],
  imports: [
      CommonModule,
      HttpClientModule,
      FormsModule,
      InfiniteScrollModule,
      NgReduxModule,
      RouterModule.forRoot(ROUTES),
  ],
})
export class AppModuleShared {
  constructor(ngRedux: NgRedux<IAppState>, devTools: DevToolsExtension) {
    const storeEnhancers = devTools.isEnabled()
      ? [devTools.enhancer()]
      : [];

    ngRedux.configureStore(rootReducer, INITIAL_STATE, [apiMiddleware], storeEnhancers);
  }
}
