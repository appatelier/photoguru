import { Injectable } from '@angular/core';
import { NgRedux } from '@angular-redux/store';
import { Action } from 'redux';
import { toPairs, fromPairs } from 'lodash';
import { IAppState } from '../models/appState';
import { ApiActionsDispatcher } from '../services/api/apiActionsDispatcher';
import { HttpRequestType } from '../models/requests/apiRequest';
import { ResourceType, IResourceConstants } from '../models/resourceConstants';
import { constantsCreator } from '../constants/constantsCreator';
import { IPagination } from '../models/requests/pagination';
import { INITIAL_PAGINATION } from '../reducers/resource';

@Injectable()
export abstract class ResourceActions<T> {
  protected constants: IResourceConstants;

  constructor(
    protected apiDispatcher: ApiActionsDispatcher,
    protected resourceType: ResourceType,
    protected endpoint: string,
  ) {
    this.constants = constantsCreator(resourceType);
  }

  index = (pagination: IPagination = INITIAL_PAGINATION) =>
    this.apiDispatcher.dispatchAuthorized<T[]>({
      endpoint: this.endpoint,
      method: HttpRequestType.Get,
      headers: { ...this.objectToHeader(pagination || {}) },
      types: [this.constants.indexRequest, this.constants.indexSuccess, this.constants.indexFailure],
    });

    private objectToHeader = (o: object)  => fromPairs(toPairs(o).map(([key, value]) => [String(key), String(value)]))
}
