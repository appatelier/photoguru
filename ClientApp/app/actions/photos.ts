import { Injectable } from '@angular/core';
import { NgRedux } from '@angular-redux/store';
import { Action } from 'redux';
import { IAppState } from '../models/appState';
import { ApiActionsDispatcher } from '../services/api/apiActionsDispatcher';
import { IPhoto } from '../models/domain/photo';
import { HttpRequestType } from '../models/requests/apiRequest';
import { ResourceActions } from './resource';
import { ResourceType } from '../models/resourceConstants';

@Injectable()
export class PhotosActions extends ResourceActions<IPhoto> {
  constructor(apiDispatcher: ApiActionsDispatcher) {
    super(apiDispatcher, ResourceType.photos, 'photos');
  }
}
