import { Injectable } from '@angular/core';
import { NgRedux } from '@angular-redux/store';
import { Action } from 'redux';
import { IAppState } from '../models/appState';

@Injectable()
export class CounterActions {
  static INCREMENT = `${CounterActions.name}/INCREMENT`;
  static DECREMENT = `${CounterActions.name}/DECREMENT`;

  constructor(private ngRedux: NgRedux<IAppState>) {}

  increment(): void {
    this.ngRedux.dispatch({ type: CounterActions.INCREMENT });
  }

  decrement(): void {
    this.ngRedux.dispatch({ type: CounterActions.DECREMENT });
  }
}
