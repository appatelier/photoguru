import { IComment } from "./comment";
import { IPhotoLike } from "./photoLike";
import { IUser } from "./user";
import { IResource } from "./resource";

export interface IPhoto extends IResource<number> {
  name: string;
  uniqueName: string;
  relativePath?: string;
  absolutePath?: string;
  length?: number;
  title?: string;
  description?: string;
  shortDescription?: string;
  style?: object | string;
  
  userId: string;
  user: IUser;
  comments: IComment[];
  likes: IPhotoLike[];
}
