import { IPhoto } from "./photo";
import { IPhotoLike } from "./photoLike";
import { IComment } from "./comment";
import { ICommentLike } from "./commentLike";
import { IResource } from "./resource";

export interface IUser extends IResource<string> {
  email: string;
  name?: string;
  picture?: string;
  nickname?: string;
  sub?: string;

  photos: IPhoto[];
  comments: IComment[];
  photoLikes: IPhotoLike[];
  commentLikes: ICommentLike[];
}
