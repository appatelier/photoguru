export interface IResource<IdType> {
  id: IdType;
}
