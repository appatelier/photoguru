import { IComment } from './comment';
import { IUser } from './user';
import { IResource } from './resource';

export interface ICommentLike extends IResource<number> {
  commentId: number;
  comment: IComment;
  userId: string;
  user: IUser;
}
