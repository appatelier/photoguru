import { ICommentLike } from './commentLike';
import { IPhoto } from "./photo";
import { IUser } from './user';
import { IResource } from './resource';

export interface IComment extends IResource<number> {
  title: string;
  message: string;

  photoId: number;
  photo: IPhoto;
  userId: string;
  user: IUser;
  likes: ICommentLike[];
}
