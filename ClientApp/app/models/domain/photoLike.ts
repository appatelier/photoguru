import { IPhoto } from './photo';
import { IUser } from './user';
import { IResource } from './resource';

export interface IPhotoLike extends IResource<number> {
  photoId: number;
  photo: IPhoto;
  userId: string;
  user: IUser;
}
