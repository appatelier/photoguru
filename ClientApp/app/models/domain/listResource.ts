import { IPagination } from '../requests/pagination';
import { ISorting } from '../requests/sorting';

export interface IListResource<T> {
  items: T[],
  pagination: IPagination,
  sorting?: ISorting | null,
}
