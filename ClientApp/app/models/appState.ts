import { ICommentLike } from './domain/commentLike';
import { IPhoto } from "./domain/photo";
import { IUser } from './domain/user';
import { IListResource } from './domain/listResource';

export interface IAppState {
  counter: number;
  photos: IListResource<IPhoto>;
  photo: IPhoto | null;
  currentUser: IUser | null;
}
