export interface IAuthConfig {
    audience: string;
    domain: string;
    clientID: string;
    callbackURL: string;
  }
  