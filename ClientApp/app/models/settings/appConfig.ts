import { IAuthConfig } from './authConfig';

export interface IAppConfig {
  authConfig: IAuthConfig
}
