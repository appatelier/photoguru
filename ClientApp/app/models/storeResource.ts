import { IListResource } from "./domain/listResource";

export interface IStoreResource<T> {
  collection: IListResource<T>,
  current: T | null,
}
