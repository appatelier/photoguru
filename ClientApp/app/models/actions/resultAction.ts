import { Action, AnyAction } from "redux";

export interface ResultAction<T> extends AnyAction {
    payload: T,
    error?: boolean,
    meta?: any,
}
