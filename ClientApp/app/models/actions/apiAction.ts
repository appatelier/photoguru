import { ApiRequest } from '../requests/apiRequest';
import { Action } from 'redux';

export interface ApiAction<T> extends Action {
   [apiRequestName: string] : ApiRequest<T>,
}
