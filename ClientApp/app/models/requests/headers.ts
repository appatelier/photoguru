import { AnyAction } from "redux";

export interface Headers {
    [headerKey: string]: string;
}
