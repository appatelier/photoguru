export interface IPagination {
    pageIndex: number;
    perPage: number;
    totalPages: number;
    hasPreviousPage?: boolean;
    hasNextPage?: boolean;
}