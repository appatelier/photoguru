import { Headers } from './headers';

export enum HttpRequestType {
    Get = 'GET',
    Head = 'HEAD',
    Post = 'POST',
    Put = 'PUT',
    Patch = 'PATCH',
    Delete = 'DELETE',
    OPTIONS = 'OPTIONS',
}

export type CredentialsType = 'omit' | 'same-origin' | 'include' ;

export interface ApiRequest<T> {
    endpoint: string,
    method: HttpRequestType,
    types: any[],
    body?: T | string | number,
    headers?: Headers,
    options?: object,
    credentials?: CredentialsType;
    bailout?: boolean,
}
