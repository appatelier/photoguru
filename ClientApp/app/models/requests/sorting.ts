import { SortDirection } from './sortDirection';
export interface ISorting {
    columnName: string,
    sortDirection: SortDirection,
}
