export enum ResourceType {
  photos = 'photos',
}

export interface IResourceConstants {
  id: ResourceType;
  endpoint: string
  indexRequest: string;
  indexSuccess: string;
  indexFailure: string;
}
