import { UrlSerializer } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { NgRedux } from '@angular-redux/store';
import { Observable } from 'rxjs/Observable';
import { RSAA } from 'redux-api-middleware'; 

import { AuthService } from '../auth/auth.service';
import { ApiHelper } from './apiHelper.service';
import { ApiRequest } from '../../models/requests/apiRequest';
import { ApiAction } from '../../models/actions/apiAction';
import { IAppState } from '../../models/appState';
import { IPagination } from '../../models/requests/pagination';

@Injectable()
export class ApiActionsDispatcher {
  private rootUrl: string;

  constructor(
    @Inject('BASE_URL') baseUrl: string,
    private apiHelper: ApiHelper,
    private ngRedux: NgRedux<IAppState>) {
      this.rootUrl = `${baseUrl}api`;
    }

  dispatch = <T> (rawRequest: ApiRequest<T>) =>
    this.ngRedux.dispatch({ [RSAA]: this.createRequest(rawRequest) } as ApiAction<T>);

  dispatchAuthorized<T>(rawRequest: ApiRequest<T>) {
    const preparedRequest = this.createRequest(rawRequest);
    const authorizedRequest: ApiRequest<T> = { 
      ...preparedRequest,
      headers: { ...preparedRequest.headers, ...this.apiHelper.plainHeaders }
    };

    this.ngRedux.dispatch({ [RSAA]: authorizedRequest } as ApiAction<T>);
  }

  private createRequest = <T> (request: ApiRequest<T>): ApiRequest<T> =>
    ({
      ...request,
      endpoint: `${this.rootUrl}/${request.endpoint}`,
    } as ApiRequest<T>)
}
