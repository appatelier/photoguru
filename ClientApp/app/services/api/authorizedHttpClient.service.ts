import { UrlSerializer } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import { AuthService } from '../auth/auth.service';
import { ApiHelper } from './apiHelper.service';

@Injectable()
export class AuthorizedHttpClient {
  constructor(
    private http: HttpClient,
    private authService: AuthService,
    private apiHelper: ApiHelper) {}

  get<T>(url: string): Observable<T> {
    return this.http.get<T>(url, { headers: this.apiHelper.headers })
  }

  post<T>(url: string, body: T): Observable<T> {
    return this.http.post<T>(url, body, { headers: this.apiHelper.headers })
  }

  put<T>(url: string, body: T): Observable<T> {
    return this.http.put<T>(url, body, { headers: this.apiHelper.headers })
  }

  delete<T>(url: string, body: T): Observable<T> {
    return this.http.request<T>('delete', url,{ headers: this.apiHelper.headers, body })
  }

  getAsync<T>(url: string): Promise<T> {
    return this.get<T>(url).toPromise();
  }

  postAsync<T>(url: string, body: T): Promise<T> {
    return this.post<T>(url, body).toPromise();
  }

  putAsync<T>(url: string, body: T): Promise<T> {
    return this.put<T>(url, body).toPromise();
  }

  deleteAsync<T>(url: string, body: T): Promise<T> {
    return this.delete<T>(url, body).toPromise();
  }
}
