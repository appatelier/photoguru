import { UrlSerializer } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';

import { AuthService } from '../auth/auth.service';
import { Headers } from '../../models/requests/headers';

@Injectable()
export class ApiHelper {
  constructor(private authService: AuthService) {}

  public get headers(): HttpHeaders {
    return new HttpHeaders(this.authHeaders);
  }

  public get plainHeaders(): Headers {
    return this.authHeaders;
  }

  private get authHeaders(): Headers {
    const token = this.authService.authToken;
    if (token == null) throw 'No token';
    return { 'Authorization': 'Bearer ' + token };
  }
}
