import { Inject, Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import * as auth0 from 'auth0-js';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';

import { IUser } from '../../models/domain/user';
import { IAppConfig } from '../../models/settings/appConfig';
import { ObjectConverterService } from '../helpers/objectConverter.service';

@Injectable()
export class AuthService {
  currentUser: IUser;
  auth0: auth0.WebAuth;

  constructor(
    public router: Router,
    private converter: ObjectConverterService,
    private http: HttpClient,
    @Inject('APP_CONFIG') private config: IAppConfig,
    @Inject('BASE_URL') private baseUrl: string
  ) {   
      const { authConfig: { clientID, domain, audience, callbackURL: redirectUri } } = config;
      this.auth0 = new auth0.WebAuth({
        clientID,
        domain,
        audience,
        redirectUri,
        responseType: 'token id_token',
        scope: 'openid profile email user_metadata'
    });
  }

  get authToken(): string | null {
    return localStorage.getItem('access_token');    
  }

  public login(): void {
    this.auth0.authorize();
  }

  public logout(): void {
    localStorage.removeItem('access_token');
    localStorage.removeItem('id_token');
    localStorage.removeItem('expires_at');
    this.router.navigate(['/unauthorized']);
  }

  public isAuthenticated(): boolean {
    const rawExpiresAt = localStorage.getItem('expires_at');
    if (!rawExpiresAt) { return false; }
    const expiresAt = JSON.parse(rawExpiresAt);
    return new Date().getTime() < expiresAt;
  }

  public async fetchProfile(): Promise<void> {
    const url = `${this.baseUrl}api/Users/Current`;
    const headers = new HttpHeaders({ 'Authorization': 'Bearer ' + this.authToken });

    this.currentUser = await this.http.get<IUser>(url, { headers }).toPromise();
  }

  public handleAuthentication(): void {
    this.auth0.parseHash(async (err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        this.setSession(authResult);
        await this.updateProfile(authResult.accessToken, authResult.idTokenPayload as IUser);
        this.router.navigate(['/']);
      } else if (err) {
        this.router.navigate(['/unauthorized']);
      }
    });
  }

  private async updateProfile (accessToken: string, user: IUser): Promise<void> {
    const preparedUser = this.converter.transformFromSnakeToCamel({ ...user, id: user.sub });
    const url = `${this.baseUrl}api/Users/Update`;
    const headers = new HttpHeaders({ 'Authorization': 'Bearer ' + accessToken });

    this.currentUser = await this.http.post<IUser>(url, preparedUser, { headers }).toPromise();
  }

  private setSession(authResult: any): void {
    const expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
    localStorage.setItem('access_token', authResult.accessToken);
    localStorage.setItem('id_token', authResult.idToken);
    localStorage.setItem('expires_at', expiresAt);
  }
}
