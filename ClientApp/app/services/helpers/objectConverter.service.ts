import { UrlSerializer } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { mapKeys, cloneDeep, mapValues, isPlainObject, camelCase, isArray, map } from 'lodash';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class ObjectConverterService {
  public transformFromSnakeToCamel (object: object): object {
    const camelCaseObject = mapKeys(cloneDeep(object), (value, key) => camelCase(key));

    return mapValues(
      camelCaseObject,
      value => {
        if (isPlainObject(value)) {
          return this.transformFromSnakeToCamel(value);
        } else if (isArray(value)) {
          return map(value, this.transformFromSnakeToCamel);
        }
        return value;
      }
    );
  }
}
