import 'rxjs/add/operator/map';

import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';

import {IUser} from '../models/domain/user';
import { AuthorizedHttpClient } from './api/authorizedHttpClient.service';

@Injectable()
export class UsersService {
  private readonly rootUrl: string;

  constructor(private http: AuthorizedHttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.rootUrl = `${baseUrl}api/Users`;
  }

  updateUser(user: IUser): Observable<IUser> {
    return this.http.post<IUser>(`${this.rootUrl}`, user);
  }

  query(): Observable<IUser[]> {
    return this.http.get<IUser[]>(this.rootUrl);
  }

  getUser(id: string): Observable<IUser> {
    return this.http.get<IUser>(`${this.rootUrl}/${id}`)
  }

  delete(user: IUser): Observable<any> {
    return this.http.delete<any>(`${this.rootUrl}/${user.id}`, user);
  }
}
