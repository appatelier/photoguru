import 'rxjs/add/operator/map';

import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';

import { IPhoto } from '../models/domain/photo';
import { AuthorizedHttpClient } from './api/authorizedHttpClient.service';

@Injectable()
export class PhotosService {
  private readonly rootUrl: string;

  constructor(private http: AuthorizedHttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.rootUrl = `${baseUrl}api/Photos`;
  }

  query(): Observable<IPhoto[]> {
    return this.http.get<IPhoto[]>(this.rootUrl);
  }

  getPhoto(id: number): Observable<IPhoto> {
    return this.http.get<IPhoto>(`${this.rootUrl}/${id}`)
  }

  delete(photo: IPhoto): Observable<any> {
    return this.http.delete<any>(`${this.rootUrl}/${photo.id}`, photo);
  }

  detectLabels(id: number): Observable<string[]> {
    return this.http.get<string[]>(`${this.rootUrl}/DetectLabels/${id}`)
  }
}
