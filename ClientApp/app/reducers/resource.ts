import { Action } from 'redux';
import { uniqBy } from 'lodash';
import { PhotosActions } from '../actions/photos';
import { IPhoto } from '../models/domain/photo';
import { ResultAction } from '../models/actions/resultAction';
import { ResourceType } from '../models/resourceConstants';
import { constantsCreator } from '../constants/constantsCreator';
import { IListResource } from '../models/domain/listResource';
import { IPagination } from '../models/requests/pagination';

export const INITIAL_PAGINATION = {
  perPage: 12,
  pageIndex: 1,
} as IPagination;

export const initialState = <T> () => ({
  items: [] as T[],
  pagination: INITIAL_PAGINATION,
} as IListResource<T>);

export const resourceReducer = <T> (resourceType: ResourceType) => {
  const constants = constantsCreator(resourceType);
  
  return (lastState: IListResource<T> = initialState<T>(), action: Action): IListResource<T> => {
    switch(action.type) {
      case constants.indexSuccess: {
        const response = (action as ResultAction<IListResource<T>>).payload;
        return {
            ...response,
            items: uniqBy([ ...lastState.items, ...response.items], 'id'),
          };
      }
    }

    return lastState;
  }
}
