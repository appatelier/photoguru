import { Action } from 'redux';
import { CounterActions } from '../actions/counter';

export const INITIAL_STATE: number = 0;

export const counterReducer = (lastState: number = INITIAL_STATE, action: Action): number => {
  switch(action.type) {
    case CounterActions.INCREMENT: return lastState + 1 ;
    case CounterActions.DECREMENT: return lastState - 1 ;
  }

  return lastState;
}
