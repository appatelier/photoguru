import { combineReducers } from 'redux';
import { composeReducers, defaultFormReducer } from '@angular-redux/form';
import { routerReducer } from '@angular-redux/router';

import { counterReducer, INITIAL_STATE as counterInitialState } from './counter';
import { IAppState } from '../models/appState';
import { initialState, resourceReducer } from './resource';
import { IPhoto } from '../models/domain/photo';
import { ResourceType } from '../models/resourceConstants';

export const INITIAL_STATE: IAppState = {
  counter: counterInitialState,
  photo: null,
  photos: initialState<IPhoto>(),
  currentUser: null,
}

export const rootReducer = composeReducers(
  defaultFormReducer(),
  combineReducers({
    counter: counterReducer,
    photos: resourceReducer<IPhoto>(ResourceType.photos),
    router: routerReducer,
  })
);
