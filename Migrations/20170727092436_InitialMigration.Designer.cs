﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Photoguru.Data;

namespace Photoguru.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20170727092436_InitialMigration")]
    partial class InitialMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "1.1.2");

            modelBuilder.Entity("Photoguru.Models.Photo", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("ImageSaved");

                    b.Property<int>("Length");

                    b.Property<string>("Name");

                    b.Property<string>("RelativePath");

                    b.Property<string>("UniqueName");

                    b.HasKey("Id");

                    b.ToTable("Photos");
                });
        }
    }
}
