using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Photoguru.Data.Services.Uploaders;
using Photoguru.Models;

namespace Photoguru.Data.Services
{
    public class ImageService
    {
        private readonly IImageUploader _imageUploader;

        public ImageService(IImageUploader imageUploader) => _imageUploader = imageUploader;

        public async Task<Photo> SaveAsync(IFormFile file) {
            var photo = Photo.Create(file);

            photo.AbsolutePath = await _imageUploader.UploadImageAsync(file, photo.UniqueName);
            photo.ImageSaved = true;

            return photo;
        }

        public IEnumerable<string> DetectLabels(Photo photo) => throw new NotImplementedException();
    }
}
