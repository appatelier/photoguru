using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Photoguru.Models;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Photoguru.Data.Services.Uploaders;

namespace Photoguru.Data.Services
{
    public class LocalStorageImageUploader : IImageUploader
    {
        private readonly string _photosDirectory;
        private readonly IHostingEnvironment _environment;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public LocalStorageImageUploader(IHostingEnvironment environment, IHttpContextAccessor httpContextAccessor)
        {
            _photosDirectory = "photos";
            _environment = environment;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<String> UploadImageAsync(IFormFile image, string id)
        {
            var photo = Photo.Create(image);
            var path = FileSystemImagePath(photo);

            using (var fileStream = new FileStream(path, FileMode.Create))
            {
                await image.CopyToAsync(fileStream);
            }

            return AbsoluteImagePath(photo);
        }

        public Task DeleteUploadedImage(string id) => throw new NotImplementedException();

        private string FileSystemImagePath(Photo photo) => Path.Combine(_environment.WebRootPath, _photosDirectory, photo.UniqueName);     
        private string AbsoluteImagePath(Photo photo) => Path.Combine(Host, _photosDirectory, photo.UniqueName);     
        private string RelativeImagePath(Photo photo) => Path.Combine(_photosDirectory, photo.UniqueName);
        private string Host {
            get {
                var request = _httpContextAccessor.HttpContext.Request;
                var protocol = request.IsHttps ? "https" : "http";
                return $@"{protocol}://{request.Host.Value}";
            }
        }
    }
}
