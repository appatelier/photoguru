using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Photoguru.Data.Services.Uploaders
{
    public interface IImageUploader
    {
        Task<String> UploadImageAsync(IFormFile image, string id);
        Task DeleteUploadedImage(string id);
    }
}
