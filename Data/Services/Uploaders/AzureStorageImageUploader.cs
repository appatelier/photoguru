using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Photoguru.Models;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Photoguru.Data.Services.Uploaders
{
    public class AzureStorageImageUploader : IImageUploader
    {
        private readonly IHostingEnvironment _environment;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly CloudBlobContainer _blobContainer;

        public AzureStorageImageUploader(IHostingEnvironment environment, IHttpContextAccessor httpContextAccessor, CloudBlobContainer blobContainer)
        {
            _environment = environment;
            _httpContextAccessor = httpContextAccessor;
            _blobContainer = blobContainer;
        }

        public async Task<String> UploadImageAsync(IFormFile image, string id)
        {
            var photo = Photo.Create(image);
            var blockBlob = _blobContainer.GetBlockBlobReference(photo.UniqueName);
                        
            using (var fileStream = image.OpenReadStream()) 
            {
                await blockBlob.UploadFromStreamAsync(fileStream);
            }

            return blockBlob.Uri.AbsoluteUri;
        }

        public Task DeleteUploadedImage(string id) => Task.FromResult(false);
    }
}
