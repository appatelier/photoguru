using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Photoguru.Data.Repositories;
using Photoguru.Data.Requests;
using Photoguru.Models;

namespace Photoguru.Data.Services.Resources
{
    public class UsersService : IResourceService<User>
    {
        private UsersRepository _usersRepository;

        public UsersService(UsersRepository usersRepository) => _usersRepository = usersRepository;

        public IEnumerable<User> GetAll() => _usersRepository.GetAll();

        public async Task<PaginatedList<User>> GetPaginated(Pagination pagination) => 
            await PaginatedList<User>.CreateAsync(_usersRepository.GetAll(), pagination?.PageIndex ?? 1, pagination.PerPage);

        public User Get(object id) => _usersRepository.Get(id);

        public User Add(User user) => _usersRepository.Add(user);

        public User Update(User user) => _usersRepository.Update(user);

        public void Delete(User user) => _usersRepository.Delete(user);

        public void DeleteAll() => _usersRepository.DeleteAll();
    }
}
