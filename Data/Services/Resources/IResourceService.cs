using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Photoguru.Data.Requests;

namespace Photoguru.Data.Services.Resources
{
    public interface IResourceService<T>
    {
        IEnumerable<T> GetAll();
        Task<PaginatedList<T>> GetPaginated(Pagination pagination);
        T Get(object id);
        T Add(T newEntity);
        T Update(T entity);
        void Delete(T entity);
        void DeleteAll();
    }
}
