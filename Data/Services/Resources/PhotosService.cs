using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Photoguru.Data.Repositories;
using Photoguru.Data.Requests;
using Photoguru.Models;

namespace Photoguru.Data.Services.Resources
{
    public class PhotosService : IResourceService<Photo>
    {
        private PhotosRepository _photosRepository;

        public PhotosService(PhotosRepository photosRepository) => _photosRepository = photosRepository;

        public IEnumerable<Photo> GetAll() => _photosRepository.GetAll();

        public async Task<PaginatedList<Photo>> GetPaginated(Pagination pagination)
        {
            var items = _photosRepository.GetAll().OrderBy(p => p.CreationDate);
            return await PaginatedList<Photo>.CreateAsync(items, pagination?.PageIndex ?? 1, pagination.PerPage);
        }

        public Photo Get(object id) => _photosRepository.Get(id);

        public Photo Add(Photo photo)
        {
            photo.CreationDate = DateTime.Now;
            return _photosRepository.Add(photo);
        } 

        public Photo Update(Photo photo) => _photosRepository.Update(photo);

        public void Delete(Photo photo) => _photosRepository.Delete(photo);

        public void DeleteAll() => _photosRepository.DeleteAll();
    }
}
