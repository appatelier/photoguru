using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Photoguru.Models;

namespace Photoguru.Data.Repositories
{
    public class UsersRepository : IRepository<User>
    {
        private ApplicationDbContext _dbContext;

        public UsersRepository(ApplicationDbContext dbContext) => _dbContext = dbContext;

        public IQueryable<User> GetAll() => _dbContext.Users.AsNoTracking();

        public User Get(object id) => _dbContext.Users.AsNoTracking().Where(u => u.Id == (string)id).FirstOrDefault();

        public User Get(Expression<Func<User, bool>> predicate) => _dbContext.Users.SingleOrDefault(predicate);
        
        public User Add(User entity)
        {
            var createdEntity = _dbContext.Add(entity);
            _dbContext.SaveChanges();

            return createdEntity.Entity;
        }

        public User Update(User entity)
        {
            _dbContext.Update(entity);
            _dbContext.SaveChanges();

            return entity;
        }

        public void Delete(User entity) 
        {
            _dbContext.Users.Remove(entity);
            _dbContext.SaveChanges();
        }

        public void DeleteAll() 
        {
            var entities = _dbContext.Users;
            _dbContext.Users.RemoveRange(entities);
            _dbContext.SaveChanges();
        }
    }
}
