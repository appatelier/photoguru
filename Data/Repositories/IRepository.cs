using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Photoguru.Data.Repositories
{
    public interface IRepository<T>
    {
        IQueryable<T> GetAll();
        T Get(object id);
        T Get(Expression<Func<T, bool>> predicate);
        T Add(T newEntity);
        T Update(T entity);
        void Delete(T entity);
        void DeleteAll();
    }
}
