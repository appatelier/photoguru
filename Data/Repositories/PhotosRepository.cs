using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Photoguru.Models;

namespace Photoguru.Data.Repositories
{
    public class PhotosRepository : IRepository<Photo>
    {
        private ApplicationDbContext _dbContext;

        public PhotosRepository(ApplicationDbContext dbContext) => _dbContext = dbContext;

        public IQueryable<Photo> GetAll() => _dbContext.Photos.AsNoTracking();

        public Photo Get(object id) => _dbContext.Photos.AsNoTracking().FirstOrDefault(p => p.Id == (int)id);

        public Photo Get(Expression<Func<Photo, bool>> predicate) => _dbContext.Photos.AsNoTracking().SingleOrDefault(predicate);

        public Photo Add(Photo entity)
        {
            var createdEntity = _dbContext.Add(entity);
            _dbContext.SaveChanges();

            return createdEntity.Entity;
        }

        public Photo Update(Photo entity)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
            _dbContext.SaveChanges();

            return entity;
        }

        public void Delete(Photo entity) 
        {
            _dbContext.Photos.Remove(entity);
            _dbContext.SaveChanges();
        }

        public void DeleteAll() 
        {
            var entities = _dbContext.Photos;
            _dbContext.Photos.RemoveRange(entities);
            _dbContext.SaveChanges();
        }
    }
}
