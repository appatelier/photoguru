using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Photoguru.Data.Requests
{
    public class Pagination
    {
        public int? PageIndex { get; set; }
        public int? PerPage { get; set; }
        public int? TotalPages { get; set; }
        public bool HasPreviousPage => PageIndex > 1;
        public bool HasNextPage => PageIndex < TotalPages;
    }
}
