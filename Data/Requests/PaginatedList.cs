using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Photoguru.Data.Requests
{
    public class PaginatedList<T>
    {
        public Pagination Pagination { get; private set; }
        public List<T> Items { get; private set; }

        public PaginatedList(List<T> items, int count, int pageIndex, int? perPage)
        {
            Items = items;            
            Pagination = new Pagination
            {
                PageIndex = pageIndex,
                PerPage = perPage,
                TotalPages = (perPage != null)
                    ? (int)Math.Ceiling(count / (double)perPage)
                    : 1
            };
        }

        public static async Task<PaginatedList<T>> CreateAsync(IQueryable<T> source, int pageIndex, int? perPage)
        {
            var count = await source.CountAsync();

            if (perPage == null)
                return new PaginatedList<T>( await source.ToListAsync(), count, 1, perPage);

            var items = await source
                .Skip((pageIndex - 1) * perPage.Value)
                .Take(perPage.Value)
                .ToListAsync();

            return new PaginatedList<T>(items, count, pageIndex, perPage);
        }
    }
}
