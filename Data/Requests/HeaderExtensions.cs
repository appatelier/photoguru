using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace Photoguru.Data.Requests
{
    public static class HeaderExtensions
    {
        public static string GetValue (this IHeaderDictionary headers, string key)
        {
            var header = headers[key];
            if (header.Count != 1) return null;

            return header.ToString();
        }

        public static int? ToNullableInt (this string value) => 
            Int32.TryParse(value, out var tempVal) ? tempVal : (int?)null;
    }
}
