using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Photoguru.Helpers
{
    public class AzureStorageProvider : Controller
    {
        private CloudStorageAccount _storageAccount;

        public AzureStorageProvider(string connectionString) => _storageAccount = CloudStorageAccount.Parse(connectionString);

        public CloudBlobContainer createContainer (string containerName) 
        {
            var client = _storageAccount.CreateCloudBlobClient();
            return client.GetContainerReference(containerName);
        }
    }
}
