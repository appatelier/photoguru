using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Photoguru.Data;
using Photoguru.Data.Repositories;
using Photoguru.Data.Services;
using Photoguru.Data.Services.Resources;
using Photoguru.Data.Services.Uploaders;
using Photoguru.Helpers;
using Microsoft.AspNetCore.Authorization;
using Photoguru.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace Photoguru
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var storageProvider = new AzureStorageProvider(Configuration["AzureStorage:Connection"]);
            var blobContainer = storageProvider.createContainer(Configuration["AzureStorage:Container"]);
            services.AddSingleton<CloudBlobContainer, CloudBlobContainer>(provider => blobContainer);

            services.AddEntityFrameworkNpgsql()
                .AddDbContext<ApplicationDbContext>(
                    options => options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection")));
                    
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IFileProvider>(
                new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot")));

            services.AddScoped<PhotosRepository, PhotosRepository>();
            services.AddScoped<UsersRepository, UsersRepository>();
            services.AddScoped<PhotosService, PhotosService>();
            services.AddScoped<UsersService, UsersService>();
            services.AddScoped<ImageService, ImageService>();
            services.AddScoped<IImageUploader, AzureStorageImageUploader>();

            string domain = $"https://{Configuration["Auth0:Domain"]}/";
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

            }).AddJwtBearer(options =>
            {
                options.Authority = domain;
                options.Audience = Configuration["Auth0:ApiIdentifier"];
            });
            
            services.AddAuthorization(options =>
            {
                options.AddPolicy("read:messages",
                    policy => policy.Requirements.Add(new HasScopeRequirement("read:messages", domain)));
            });

            services.AddSingleton<IAuthorizationHandler, HasScopeHandler>();

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                {
                    HotModuleReplacement = true
                });
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new { controller = "Home", action = "Index" });
            });
        }
    }
}
